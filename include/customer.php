<?php
class magento_customer extends magento
{
  public function __construct()
  {
    parent::__construct();
    $this->session = $this->magento_session();
  }

  public function create_customer()
  {
    $customer = array(
      'firstname' => $this->nome_comprador,
      'lastname' => $this->sobrenome_comprador,
      'email' => $this->email_comprador,
      'telephone' => $this->telefone_comprador['0'],
      'taxvat' => $this->numero_documento_comprador,
      'group_id' => "1",
      'store_id' => STORE_ID,
      'website_id' => "3"
    );

    $complexFilter = array(
      'complex_filter' => array(
        array(
          'key' => 'taxvat',
          'value' => array('key' => 'in', 'value' => $this->numero_documento_comprador)
        )
      )
    );

    $return = $this->soap_client->customerCustomerList($this->session, $complexFilter);
    if(!$return) {
      try {
        $this->id_customer = $this->soap_client->customerCustomerCreate($this->session, $customer);
        $this->create_customer_address();
        return $this->id_customer;
      } catch (Exception $e) {
        echo "Problema: ". $e->getMessage() . "\n";
        return false;
      }
    } else {
      $this->id_customer = $return[0]->customer_id;
      return $this->id_customer;
    }
  }

  public function create_customer_address()
  {
    $customer_address = array(
      'firstname' => $this->nome_comprador,
      'lastname' => $this->sobrenome_comprador,
      'street' => array($this->billing_rua.", ".$this->billing_numero." - ".$this->billing_bairro,''),
      'city' => $this->billing_cidade,
      'country_id' => $this->billing_pais,
      'region' => $this->billing_estado,
      'postcode' => $this->billing_cep,
      'telephone' => $this->telefone_comprador['0'],
      'is_default_billing' => TRUE,
      'is_default_shipping' => TRUE
    );

    try {
      $return = $this->soap_client->customerAddressCreate($this->session, $this->id_customer, $customer_address);

      return $return;
    } catch (Exception $e) {
      echo "Problema: ". $e->getMessage() . "\n";
      return false;
    }
  }

  function get_customer_info($customer_id, $params = null)
  {
    return $this->soap_client->customerCustomerInfo($this->session, $customer_id, $params);
  }

  function get_customer_address_list($customer_id)
  {
    return $this->soap_client->customerAddressList($this->session, $customer_id);
  }

  function get_customer_list($param = null)
  {
    return $this->soap_client->customerCustomerList($this->session, $param);
  }
}

?>
