<?php
class magento_product extends magento
{
  public function __construct()
  {
    parent::__construct();
    $this->session = $this->magento_session();
  }

  public function update_product_stock($sku,$qty)
  {
    $is_in_stock = 1;
    if($qty <= 0) $is_in_stock = 0;

    $mod = array(
      'qty' => $qty,
      'is_in_stock' => $is_in_stock
    );

    $return = $this->soap_client->catalogInventoryStockItemUpdate($this->session,$sku,$mod);

    return $return;
  }

  public function update_product($sku,$mod)
  {
    if(isset($mod['additional_attributes'])) {
      foreach ($mod['additional_attributes']['single_data'] as $key => $value) {
        try {
          $this->get_product_attribute($value['key']);
        } catch (Exception $e) {
          $this->createSetAttribute($value['key']);
        }
      }
    }
    // 'additional_attributes' => array(
    //     'single_data' => array(
    //       array('key'=> 'lenght', 'value' => '7'),
    //             array('key'=> 'height', 'value' => '7'),
    //             array('key'=> 'width', 'value' => '7')))
    $return = $this->soap_client->catalogProductUpdate($this->session,$sku,$mod);

    return $return;
  }

  public function update_product_price($sku,$tier_price)
  {
    $update_tier_price = $this->soap_client->catalogProductAttributeTierPriceUpdate($this->session,$sku, $tier_price);

    return $update_tier_price;
  }

  public function get_products()
  {
    $result = $this->soap_client->catalogProductList($this->session);

    return $result;
  }

  public function get_product_list()
  {
    $products = $this->get_products();
    $list_product_id = [];
    foreach ($products as $key => $value) $list_product_id[] = $value->sku;

    return $list_product_id;
  }

  public function get_product_info($sku)
  {
    $param = array('additional_attributes' => array('length','width','height'));

    $result = $this->soap_client->catalogProductInfo($this->session,$sku,null,$param);

    if(isset($result->additional_attributes)) {
      foreach ($result->additional_attributes as $key => $value) {
        if($value->key == 'length') {

        } else if($value->key == 'width') {

        } else if($value->key == 'height') {

        } else {
          $this->createSetAttribute($value->key);
        }
      }
    }
    
    return $result;
  }

  public function get_product_stock_list($sku)
  {
    $sku_list = array($sku);
    $result = $this->soap_client->catalogInventoryStockItemList($this->session,$sku_list);

    return $result;
  }

  public function get_product_images($sku)
  {
    $medialist = $this->soap_client->catalogProductAttributeMediaList($this->session,$sku);

    return $medialist;
  }

  public function get_list_product_images($sku)
  {
    $medialist = $this->get_product_images($sku);
    $list_images = [];
    foreach ($medialist as $key => $value) $list_images[] = $value->url;

    return $list_images;
  }

  public function create_product_attribute($data)
  {
    $id_attribute = $this->soap_client->catalogProductAttributeCreate($this->session,$data);
    return $id_attribute;
  }

  public function set_product_attribute($attribute_id)
  {
    return $this->soap_client->catalogProductAttributeSetAttributeAdd($this->session,$attribute_id,'4');
  }

  public function createSetAttribute($attributeName)
  {
    $attributeEntities = array(
      "attribute_code" => $attributeName,
      "frontend_input" => "text",
      "scope" => "global",
      "default_value" => "1",
      "is_unique" => 0,
      "is_required" => 1,
      "apply_to" => array(),
      "is_configurable" => 0,
      "is_searchable" => 0,
      "is_visible_in_advanced_search" => 0,
      "is_comparable" => 0,
      "is_used_for_promo_rules" => 0,
      "is_visible_on_front" => 1,
      "used_in_product_listing" => 1,
      "additional_fields" => array(),
      "frontend_label" => array(array("store_id" => "0", "label" => $attributeName))
    );
    $attributeId = $this->create_product_attribute($attributeEntities);

    $setAttribute = $this->set_product_attribute($attributeId);

    if(!$setAttribute) return false;

    return $setAttribute;
  }

  public function get_product_attribute($attribute_id)
  {
    return $this->soap_client->catalogProductAttributeInfo($this->session,$attribute_id);
  }

  public function get_product_attribute_list($product_type)
  {
    return $this->soap_client->catalogProductListOfAdditionalAttributes($this->session,$product_type,4);
  }
}
?>
