<?php

class magento_order extends magento_customer
{
  public function __construct()
  {
    parent::__construct();
  }

  public function set_order_info($order_data)
  {
    $this->id_order = $order_data->id_order;
    $this->sku_produto = $order_data->sku_produto;
    $this->nome_produto = $order_data->nome_produto;
    $this->qtd_produto = $order_data->qtd_produto;
    $this->preco_especial_produto = $order_data->preco_especial_produto;
    $this->preco_original_produto = $order_data->preco_original_produto;
    //--------------PAGAMENTO---------
    $this->tipo_pagamento = $order_data->tipo_pagamento;
    $this->custo_envio = $order_data->custo_envio;
    $this->total_pagar = $order_data->total_pagar;
    $this->desconto = $order_data->desconto;
    $this->parcels = $order_data->parcels;
    //-----------ENDEREÇO ENTREGA---------
    $this->shipping_receptor = $order_data->shipping_receptor;
    $this->shipping_rua = $order_data->shipping_rua;
    $this->shipping_numero = $order_data->shipping_numero;
    $this->shipping_bairro = $order_data->shipping_bairro;
    $this->shipping_cep = $order_data->shipping_cep;
    $this->shipping_cidade = $order_data->shipping_cidade;
    $this->shipping_estado = $order_data->shipping_estado;
    $this->shipping_pais = $order_data->shipping_pais;
    $this->shipping_phone = $order_data->shipping_phone;
    $this->shipping_referencia = $order_data->shipping_referencia;

    //-----------ENDEREÇO COBRANÇA---------
    $this->billing_receptor = $order_data->billing_receptor;
    $this->billing_rua = $order_data->billing_rua;
    $this->billing_numero = $order_data->billing_numero;
    $this->billing_bairro = $order_data->billing_bairro;
    $this->billing_cep = $order_data->billing_cep;
    $this->billing_cidade = $order_data->billing_cidade;
    $this->billing_estado = $order_data->billing_estado;
    $this->billing_pais = $order_data->billing_pais;
    $this->billing_phone = $order_data->billing_phone;
    $this->billing_referencia = $order_data->billing_referencia;
    // ---------USUARIO---------
    $this->email_comprador = $order_data->email_comprador;
    $this->telefone_comprador = $order_data->telefone_comprador;
    $this->nome_comprador = $order_data->nome_comprador;
    $this->sobrenome_comprador = $order_data->sobrenome_comprador;
    $this->numero_documento_comprador = $order_data->numero_documento_comprador;
    $this->nascimento = $order_data->nascimento;
  }

  public function get_orders()
  {
    $return = $this->soap_client->salesOrderList($this->session);

    return $return;
  }

  public function get_order_list()
  {
    // try {
      $orders = $this->get_orders();
      $orders_id = [];
      foreach ($orders as $key => $value) {
        if($value->status == 'complete') $orders_id[] = $value->increment_id;
      }
      return $orders_id;
    // } catch(Exception $e) {
    //   return $e->getMessage();
    // }
  }

  public function get_order_info($increment_id)
  {
    return $this->soap_client->salesOrderInfo($this->session, $increment_id);
  }

  public function get_last_order()
  {
    $sales_order = $this->get_order_list();

    $last_sales_order = end($sales_order);

    return $last_sales_order;
  }

  public function create_cart()
  {
    // try {
      $this->cart_id = $this->soap_client->shoppingCartCreate($this->session, STORE_ID);
      return $this->cart_id;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function add_product_cart()
  {
    // try {
    if(gettype($this->sku_produto) == 'array') {
      foreach ($this->sku_produto as $key => $value) {
        $shoppingCartProductEntity[$key] = array(
          'sku' => $this->sku_produto[$key],
          'qty' => $this->qtd_produto[$key]);
      }
    } else {
      $shoppingCartProductEntity[] = array(
        'sku' => $this->sku_produto,
        'qty' => $this->qtd_produto);
    }


      $return = $this->soap_client->shoppingCartProductAdd($this->session, $this->cart_id, $shoppingCartProductEntity, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function get_product_cart_list()
  {
    // try {
      $return = $this->soap_client->shoppingCartProductList($this->session, $this->cart_id, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function add_customer_cart()
  {
    // try {
    $customer = array(
      'customer_id' => $this->id_customer,
      'mode' => "customer"
    );


      $return = $this->soap_client->shoppingCartCustomerSet($this->session, $this->cart_id, $customer, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo $e->getMessage();
    //   return false;
    // }
  }

  public function add_customer_address_cart()
  {
    $nome_sobrenome_billing = "B2W-".ucwords(strtolower($this->billing_receptor));
    $nome_billing = explode(' ', $nome_sobrenome_billing);
    $sobrenome_billing = array_splice($nome_billing, -1);
    $nome_billing = implode(' ',$nome_billing);
    $sobrenome_billing = implode(' ',$sobrenome_billing);

    $nome_sobrenome_shipping = "B2W-".ucwords(strtolower($this->shipping_receptor));
    $nome_shipping = explode(' ', $nome_sobrenome_shipping);
    $sobrenome_shipping = array_splice($nome_shipping, -1);
    $nome_shipping = implode(' ',$nome_shipping);
    $sobrenome_shipping = implode(' ', $sobrenome_shipping);

    $billing = array(
      array(
        'mode' => 'billing',
        'firstname' => $nome_billing,
        'lastname' => $sobrenome_billing,
        'street' => $this->billing_rua.", ".$this->billing_numero." - ".$this->billing_bairro,
        'city' => $this->billing_cidade,
        'region' => $this->billing_estado,
        'postcode' => $this->billing_cep,
        'country_id' => $this->billing_pais,
        'telephone' => $this->billing_phone,
        'is_default_billing' => TRUE,
        'is_default_shipping' => FALSE),
      array(
        'mode' => 'shipping',
        'firstname' => $nome_shipping ,
        'lastname' => $sobrenome_shipping,
        'street' => $this->shipping_rua.", ".$this->shipping_numero." - ".$this->shipping_bairro,
        'city' => $this->shipping_cidade,
        'region' => $this->shipping_estado,
        'postcode' => $this->shipping_cep,
        'country_id' => $this->shipping_pais,
        'telephone' => $this->shipping_phone,
        'is_default_billing' => FALSE,
        'is_default_shipping' => TRUE)
    );

    // try {
      $return = $this->soap_client->shoppingCartCustomerAddresses($this->session, $this->cart_id, $billing, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function add_shipping_cart()
  {
    // try {
      $return = $this->soap_client->shoppingCartShippingMethod($this->session, $this->cart_id, SHIPPING_METHOD, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function add_payment_cart()
  {
    $payment = array(
      'po_number' => null,
      'method' => 'cashondelivery',
      'cc_cid' => null,
      'cc_owner' => null,
      'cc_number' => null,
      'cc_type' => null,
      'cc_exp_year' => null,
      'cc_exp_month' => null
    );

    // try {
      $return =  $this->soap_client->shoppingCartPaymentMethod($this->session, $this->cart_id, $payment, STORE_ID);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function create_order()
  {
    // try {
      $this->order_id = $this->soap_client->shoppingCartOrder($this->session, $this->cart_id, STORE_ID);

      return $this->order_id;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function add_order_comment($order_id, $comment)
  {
    // try {
      $return = $this->soap_client->salesOrderAddComment($this->session, $order_id, 'complete', $comment, null);

      return $return;
    // } catch (SoapFault $e) {
    //   echo "Problema: ". $e->getMessage() . "\n";
    //   return false;
    // }
  }

  public function createOrderMagento()
  {
      if(!$this->create_customer()) return false;

      if(!$this->create_cart()) return false;

      if(!$this->add_product_cart()) return false;

      if(!$this->get_product_cart_list()) return false;

      if(!$this->add_customer_cart()) return false;

      if(!$this->add_customer_address_cart()) return false;

      if(!$this->add_shipping_cart()) return false;

      if(!$this->add_payment_cart()) return false;

      if(!$this->create_order()) return false;

      $comment = "Id do Pedido B2W:".$this->id_order.
      "->Referencia do local de entrega:".$this->shipping_referencia.
      "->Referencia do local de entrega do pagamento:".$this->billing_referencia.
      "->Tipo de pagamento:".$this->tipo_pagamento.
      "->Preço original do produto:".$this->preco_original_produto.
      "->Preço promocional do produto:".$this->preco_especial_produto.
      "->Subtotal:".(float)$this->preco_especial_produto*(float)$this->qtd_produto.
      "->Desconto:".$this->desconto.
      "->Custo Frete:".$this->custo_envio.
      "->Valor Total:".$this->total_pagar.
      "->Data de nascimento:".$this->nascimento.
      "->Parcelas:".$this->parcels;

      if(!$this->add_order_comment($this->order_id,$comment)) return false;

      return (int)$this->order_id;
  }
}
