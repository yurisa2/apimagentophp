<?php

class magento_orders extends magento{
  /**
  * Construtor. Set properties in Magento_order
  * @param object $dadosVenda;
  */

  public function __construct($dadosVenda)
  {
    parent::__construct();
    $this->store_id = STORE_ID;

    $this->obj_session = magento_session();

    $this->id_order = $dadosVenda->id_order;
    $this->sku_produto = $dadosVenda->sku_produto;
    $this->nome_produto = $dadosVenda->nome_produto;
    $this->qtd_produto = $dadosVenda->qtd_produto;
    $this->preco_especial_produto = $dadosVenda->preco_especial_produto;
    $this->preco_original_produto = $dadosVenda->preco_original_produto;
    //--------------PAGAMENTO---------
    $this->tipo_pagamento = $dadosVenda->tipo_pagamento;
    $this->custo_envio = $dadosVenda->custo_envio;
    $this->total_pagar = $dadosVenda->total_pagar;
    $this->desconto = $dadosVenda->desconto;
    $this->parcels = $dadosVenda->parcels;
    //-----------ENDEREÇO ENTREGA---------
    $this->shipping_receptor = $dadosVenda->shipping_receptor;
    $this->shipping_rua = $dadosVenda->shipping_rua;
    $this->shipping_numero = $dadosVenda->shipping_numero;
    $this->shipping_bairro = $dadosVenda->shipping_bairro;
    $this->shipping_cep = $dadosVenda->shipping_cep;
    $this->shipping_cidade = $dadosVenda->shipping_cidade;
    $this->shipping_estado = $dadosVenda->shipping_estado;
    $this->shipping_pais = $dadosVenda->shipping_pais;
    $this->shipping_phone = $dadosVenda->shipping_phone;
    $this->shipping_referencia = $dadosVenda->shipping_referencia;

    //-----------ENDEREÇO COBRANÇA---------
    $this->billing_receptor = $dadosVenda->billing_receptor;
    $this->billing_rua = $dadosVenda->billing_rua;
    $this->billing_numero = $dadosVenda->billing_numero;
    $this->billing_bairro = $dadosVenda->billing_bairro;
    $this->billing_cep = $dadosVenda->billing_cep;
    $this->billing_cidade = $dadosVenda->billing_cidade;
    $this->billing_estado = $dadosVenda->billing_estado;
    $this->billing_pais = $dadosVenda->billing_pais;
    $this->billing_phone = $dadosVenda->billing_phone;
    $this->billing_referencia = $dadosVenda->billing_referencia;
    // ---------USUARIO---------
    $this->email_comprador = $dadosVenda->email_comprador;
    $this->telefone_comprador = $dadosVenda->telefone_comprador;
    $this->nome_comprador = $dadosVenda->nome_comprador;
    $this->sobrenome_comprador = $dadosVenda->sobrenome_comprador;
    $this->numero_documento_comprador = $dadosVenda->numero_documento_comprador;
    $this->nascimento = $dadosVenda->nascimento;
  }

  public function customerCustomerCreate()
  {
    $customer = array(
      'firstname' => $this->nome_comprador,
      'lastname' => $this->sobrenome_comprador,
      'email' => $this->email_comprador,
      'telephone' => $this->telefone_comprador['0'],
      'taxvat' => $this->numero_documento_comprador,
      'group_id' => "1",
      'store_id' => STORE_ID,
      'website_id' => "3"
    );

    $complexFilter = array(
      'complex_filter' => array(
        array(
          'key' => 'taxvat',
          'value' => array('key' => 'in', 'value' => $customer['taxvat'])
        )
      )
    );

    $return = $this->soap_client->customerCustomerList($this->session, $complexFilter);
    //VERIFICAÇÃO SE EXISTE CLIENTE CADASTRADO COM O EMAIL NO MGNT
    //CASO NÃO EXISTA É CADASTRADO E É PEGO O ID DO CLIENTE
    if(!$return) {
      // function ustomerCustomerCreate()
      $id_customer = $this->soap_client->customerCustomerCreate($this->session, $customer);
      if($id_customer) echo "Customer Cadastrado com sucesso->ID: ".$id_customer;

      $customer_address = array(
        'firstname' => $this->nome_comprador,
        'lastname' => $this->sobrenome_comprador,
        'street' => array($this->billing_rua.", ".$this->billing_numero." - ".$this->billing_bairro,''),
        'city' => $this->billing_cidade,
        'country_id' => $this->billing_pais,
        'region' => $this->billing_estado,
        'postcode' => $this->billing_cep,
        'telephone' => $this->telefone_comprador['0'],
        'is_default_billing' => TRUE,
        'is_default_shipping' => TRUE
      );

      $return = $this->soap_client->customerAddressCreate($this->session, $id_customer, $customer_address);

      //Se na requisição para atualizar o produto houver problema (retorno dif de 200)
      // ele entra no bloco de código
      if(gettype($return) !== 'integer') {
        $nome_funcao = "magento1_customerCustomerCreate";
        $saida = $return->faultstring;
        $titulo = "Erro no Script Integração SKYHUB Magento";
        mandaEmail_files_db($nome_funcao,$saida,$titulo);
        return false;
      } else {
        echo "Criado customer Address";
        return $id_customer;
      }
    } else {
      $id_customer = $return[0]->customer_id;
      echo "Id customer::: ";
      return $id_customer;
    }
  }

  public function shoppingCartCreate()
  {
    $cart_id = $this->soap_client->shoppingCartCreate($this->session, $this->store_id);

    if($cart_id) echo "<br/>ID do Carrinho de Compras: ".$cart_id;
    else{
      $nome_funcao = "magento3_shoppingCartCreate";
      $saida = $cart_id->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      return false;
    }
    return $cart_id;
  }

  public function shoppingCartProductAdd($cart_id)
  {
    if(gettype($this->sku_produto) == 'array') {
      foreach ($this->sku_produto as $key => $value) {
        $shoppingCartProductEntity[$key] = array(
          'sku' => $this->sku_produto[$key],
          'qty' => $this->qtd_produto[$key]);
      }
    } else {
      $shoppingCartProductEntity[] = array(
        'sku' => $this->sku_produto,
        'qty' => $this->qtd_produto);
    }

    $result_prod_add = $this->soap_client->shoppingCartProductAdd($this->session, $cart_id, $shoppingCartProductEntity, $this->store_id);
    var_dump($result_prod_add);
    if ($result_prod_add === true){
      echo "<br/>Itens adicionados no Carrinho: ";
      return 1;
    } else {
      echo "<br/>Produtos não puderam ser adicionados";var_dump($result_prod_add);
      $nome_funcao = "magento4_shoppingCartProductAdd";
      $saida = $result_prod_add->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      return false;
    }
  }

  public function shoppingCartProductList($cart_id)
  {
    $result = $this->soap_client->shoppingCartProductList($this->session, $cart_id, $this->store_id);
    //Se na requisição para atualizar o produto houver problema (retorno dif de 200)
    // ele entra no bloco de código
    if(gettype($result) != 'array') {
      $nome_funcao = "magento5_shoppingCartProductList";
      $saida = $result->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      return false;
    } else return "Produtos adicionados no carrinho";
  }

  public function shoppingCartCustomerSet($cart_id, $id_customer)
  {
    $customer = array(
      'customer_id' => $id_customer,
      'mode' => "customer"
    );

    $return = $this->soap_client->shoppingCartCustomerSet($this->session, $cart_id, $customer, $this->store_id);
    if ($return == true) return "Setado Customer com sucesso: ";
    else {
      $nome_funcao = "magento6_shoppingCartCustomerSet";
      $saida = $return->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo "<br/>Não foi possível Setar Customer";
      return false;
    }
  }

  public function shoppingCartCustomerAddresses($cart_id)
  {
    $nome_sobrenome_billing = "B2W-".ucwords(strtolower($this->billing_receptor));
    $nome_billing = explode(' ', $nome_sobrenome_billing);
    $sobrenome_billing = array_splice($nome_billing, -1);
    $nome_billing = implode(' ',$nome_billing);
    $sobrenome_billing = implode(' ',$sobrenome_billing);

    $nome_sobrenome_shipping = "B2W-".ucwords(strtolower($this->shipping_receptor));
    $nome_shipping = explode(' ', $nome_sobrenome_shipping);
    $sobrenome_shipping = array_splice($nome_shipping, -1);
    $nome_shipping = implode(' ',$nome_shipping);
    $sobrenome_shipping = implode(' ', $sobrenome_shipping);

    $billing = array(
      array(
        'mode' => 'billing',
        'firstname' => $nome_billing,
        'lastname' => $sobrenome_billing,
        'street' => $this->billing_rua.", ".$this->billing_numero." - ".$this->billing_bairro,
        'city' => $this->billing_cidade,
        'region' => $this->billing_estado,
        'postcode' => $this->billing_cep,
        'country_id' => $this->billing_pais,
        'telephone' => $this->billing_phone,
        'is_default_billing' => TRUE,
        'is_default_shipping' => FALSE),
      array(
        'mode' => 'shipping',
        'firstname' => $nome_shipping ,
        'lastname' => $sobrenome_shipping,
        'street' => $this->shipping_rua.", ".$this->shipping_numero." - ".$this->shipping_bairro,
        'city' => $this->shipping_cidade,
        'region' => $this->shipping_estado,
        'postcode' => $this->shipping_cep,
        'country_id' => $this->shipping_pais,
        'telephone' => $this->shipping_phone,
        'is_default_billing' => FALSE,
        'is_default_shipping' => TRUE)
    );

    $return = $this->soap_client->shoppingCartCustomerAddresses($this->session, $cart_id, $billing, $this->store_id);

    if ($return === true) return "Setado Customer Addresses no carrinho";
    else {
      $nome_funcao = "magento7_shoppingCartCustomerAddresses";
      $saida = $return->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo "Nao deu para setar Address no carrinho";
      return false;
    }
  }

  public function shoppingCartShippingMethod($cart_id)
  {
    $return = $this->soap_client->shoppingCartShippingMethod($this->session, $cart_id, $shipping_method, $this->store_id);

    if ($return == true) return "Setado Shipping Method para o carrinho".var_dump($return);
    else {
      $nome_funcao = "magento8_shoppingCartShippingMethod";
      $saida = $return->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo "Não foi possivel acionar o metodo de entrega";
      return false;
    }
  }

  public function shoppingCartPaymentMethod($cart_id)
  {
    $payment = array(
      'po_number' => null,
      'method' => 'cashondelivery',
      'cc_cid' => null,
      'cc_owner' => null,
      'cc_number' => null,
      'cc_type' => null,
      'cc_exp_year' => null,
      'cc_exp_month' => null
    );

    $return =  $this->soap_client->shoppingCartPaymentMethod($this->session, $cart_id, $payment, $this->store_id);

    if ($return == true) return "Setado Payment Method para o carrinho<br/>";
    else {
      $nome_funcao = "magento9_shoppingCartPaymentMethod";
      $saida = $return->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo "Problema meio de pagamento";
      return false;
    }
  }

  public function shoppingCartOrder($cart_id)
  {
    $order_id = $this->soap_client->shoppingCartOrder($this->session, $cart_id, $this->store_id);
    if(strlen($order_id) < 11) echo "<br/>Order criado - ".$order_id;
    else {
      $nome_funcao = "magento10_shoppingCartOrder";
      $saida = $order_id->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo '<br/>Deu problema no final--> '.$order_id;
      return false;
    }

    $comment = "Id do Pedido B2W:".$this->id_order.
    "->Referencia do local de entrega:".$this->shipping_referencia.
    "->Referencia do local de entrega do pagamento:".$this->billing_referencia.
    "->Tipo de pagamento:".$this->tipo_pagamento.
    "->Preço original do produto:".$this->preco_original_produto.
    "->Preço promocional do produto:".$this->preco_especial_produto.
    "->Subtotal:".(float)$this->preco_especial_produto*(float)$this->qtd_produto.
    "->Desconto:".$this->desconto.
    "->Custo Frete:".$this->custo_envio.
    "->Valor Total:".$this->total_pagar.
    "->Data de nascimento:".$this->nascimento.
    "->Parcelas:".$this->parcels;

    $return = $this->soap_client->salesOrderAddComment($this->session, $order_id, 'pending', $comment, null);
    if($return == true) echo "<br/>Comentário criado<br/>";
    else {
      $nome_funcao = "magento10_shoppingCartOrder: comentário";
      $saida = $return->faultstring;
      $titulo = "Erro no Script Integração SKYHUB Magento";
      mandaEmail_files_db($nome_funcao,$saida,$titulo);
      echo "Não foi possivel adicionar comentario<br/>";
      return false;
    }

    if((strlen($order_id) < 11) && ($return == true)) return $order_id;
    else return false;
  }

  public function createOrderMagento()
  {
    $this->customer_id = $this->customerCustomerCreate();
    if(!$this->customer_id) return false;

    $this->cart_id = $this->shoppingCartCreate();
    if(!$this->cart_id) return false;

    $product_add = $this->shoppingCartProductAdd($this->cart_id);
    if(!$product_add) return false;;

    $product_list = $this->shoppingCartProductList($this->cart_id);
    if(!$product_list) return false;

    $customer_set = $this->shoppingCartCustomerSet($this->cart_id,$this->customer_id);
    if(!$customer_set) return false;

    $customer_addresses = $this->shoppingCartCustomerAddresses($this->cart_id);
    if(!$customer_addresses) return false;

    $shipping_method = $this->shoppingCartShippingMethod($this->cart_id);
    if(!$shipping_method) return false;

    $payment_method = $this->shoppingCartPaymentMethod($this->cart_id);
    if(!$payment_method) return false;

    $cart_order = $this->shoppingCartOrder($this->cart_id);
    if(!$cart_id) return false;

    return $cart_order;
  }
}
